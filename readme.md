# Krown

A distributed pipeline system written in Go

## The goal

Krown is a pipeline system designed to be as generic as possible, allowing to use the software you want where you want.                                                                      

## Structure

The system is divided in a manager and a group of workers:

- The manager deals with pipeline authentication, pipeline configurations and workers registration. It controls the jobs assignment for the workers.                                         
- The workers are minimal systems that wait for a command from the manager, execute the job and returns the results. Those workers are designed to be able to run both native code than dockerized applications.

